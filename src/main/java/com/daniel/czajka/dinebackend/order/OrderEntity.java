package com.daniel.czajka.dinebackend.order;

import com.daniel.czajka.dinebackend.restaurant.RestaurantEntity;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;


@Entity
@Table(name = "restaurant_order")
@Data
public class OrderEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "restaurant_id", nullable = false)
    private RestaurantEntity restaurant;

    @Column(name = "table_id")
    private Long tableId;

    @Column(name = "created_timestamp")
    private Instant CreatedDateTime;

    @Column(name = "event_time")
    private Instant eventTime;
}
