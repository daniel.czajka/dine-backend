package com.daniel.czajka.dinebackend.restaurant;


import com.daniel.czajka.dinebackend.order.OrderEntity;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;


@Entity
@Table(name = "restaurant")
@Data
public class RestaurantEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "restaurant_name")
    private String restaurantName;

    @Column(name = "restaurant_address")
    private String restaurantAddress;

    @Column(name = "restaurant_email")
    private String restaurantEmail;

    @Column(name = "restaurantPhone")
    private String restaurantPhone;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "restaurant")
    private Set<OrderEntity> orders;
}
