package com.daniel.czajka.dinebackend.restaurant;

import com.daniel.czajka.dinebackend.restaurant.exceptions.RestaurantNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RestaurantService {

    private final RestaurantRepository restaurantRepository;

    @Autowired
    public RestaurantService(RestaurantRepository restaurantRepository) {
        this.restaurantRepository = restaurantRepository;
    }

    public RestaurantEntity addRestaurant(RestaurantEntity restaurantEntity) {
        return restaurantRepository.save(restaurantEntity);
    }

    public List<RestaurantEntity> findAllRestaurants() {
        return restaurantRepository.findAll();
    }

    public RestaurantEntity findRestaurantById(Long id) {
        return restaurantRepository.findRestaurantById(id)
                .orElseThrow(() -> new RestaurantNotFoundException("Restaurant by id not found"));
    }

    public RestaurantEntity updateRestaurant(RestaurantEntity restaurantEntity) {
        return restaurantRepository.save(restaurantEntity);
    }

    public void deleteResturant(Long id) {
        restaurantRepository.deleteRestaurantById(id);
    }

}
