package com.daniel.czajka.dinebackend.restaurant.dto;

import com.daniel.czajka.dinebackend.restaurant.RestaurantEntity;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Builder
@Getter
public class RestaurantDTO {

    private Long id;
    private String restaurantName;
    private String restaurantAddress;
    private String restaurantEmail;
    private String restaurantPhone;


}
