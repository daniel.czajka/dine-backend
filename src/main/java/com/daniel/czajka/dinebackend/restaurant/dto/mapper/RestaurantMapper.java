package com.daniel.czajka.dinebackend.restaurant.dto.mapper;

import com.daniel.czajka.dinebackend.restaurant.RestaurantEntity;
import com.daniel.czajka.dinebackend.restaurant.dto.RestaurantDTO;

public class RestaurantMapper {
    public static RestaurantDTO getDefinitionFromEntity(RestaurantEntity entity) {
        if (entity == null) {
            return null;
        }
        return RestaurantDTO.builder()
                .id(entity.getId())
                .restaurantName(entity.getRestaurantName())
                .restaurantAddress(entity.getRestaurantAddress())
                .restaurantEmail(entity.getRestaurantEmail())
                .restaurantPhone(entity.getRestaurantPhone())
                .build();
    }
}
