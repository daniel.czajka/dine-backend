package com.daniel.czajka.dinebackend.restaurant;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RestaurantRepository extends JpaRepository<RestaurantEntity, Long> {

    void deleteRestaurantById(Long id);

    Optional<RestaurantEntity> findRestaurantById(Long id);
}
