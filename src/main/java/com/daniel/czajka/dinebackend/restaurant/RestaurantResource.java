package com.daniel.czajka.dinebackend.restaurant;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/restaurant")
public class RestaurantResource {

    private final RestaurantService restaurantService;

    public RestaurantResource(RestaurantService restaurantService) {
        this.restaurantService = restaurantService;
    }

    @GetMapping("/find/all")
    public ResponseEntity<List<RestaurantEntity>> getAllRestaurant() {
        List<RestaurantEntity> restaurants = restaurantService.findAllRestaurants();
        return new ResponseEntity<>(restaurants, HttpStatus.OK);
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<RestaurantEntity> getRestaurantById(@PathVariable("id") Long id) {
        RestaurantEntity restaurant = restaurantService.findRestaurantById(id);
        return new ResponseEntity<>(restaurant, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<RestaurantEntity> addRestaurant(@RequestBody RestaurantEntity newRestaurant) {
        RestaurantEntity restaurant = restaurantService.addRestaurant(newRestaurant);
        return new ResponseEntity<>(restaurant, HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<RestaurantEntity> updateRestaurant(@RequestBody RestaurantEntity updatedRestaurant) {
        RestaurantEntity restaurant = restaurantService.updateRestaurant(updatedRestaurant);
        return new ResponseEntity<>(restaurant, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteRestaurantById(@PathVariable("id") Long id) {
         restaurantService.deleteResturant(id);
         return new ResponseEntity<>(HttpStatus.OK);
    }
}
